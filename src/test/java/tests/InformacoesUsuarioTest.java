package tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class InformacoesUsuarioTest {
    private  WebDriver navegador;
    @Before
    public void SetUp(){
        //Abrindo o navegador maximizado
        System.setProperty("webdriver.chrome.driver", "D:\\drivers\\chromedriver_win32\\chromedriver.exe");
        navegador = new ChromeDriver();
        navegador.manage().window().maximize();
        navegador.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        //Navegar para o taskit
        navegador.get("http://www.juliodelima.com.br/taskit");

        //Clicar no campo com texto "Sign in"
        navegador.findElement(By.linkText("Sign in")).click();

        //Encontrando o formulario signinbox
        WebElement formSigninBox = navegador.findElement(By.id("signinbox"));

        //Digitar no campo com name "login" dentro do formulário de id "signinbox" o login "julio0001"
        formSigninBox.findElement(By.name("login")).sendKeys("julio0001");

        //Digitar no campo com name "password" dentro do formulário de id "signinbox" o login "123456"
        formSigninBox.findElement(By.name("password")).sendKeys("123456");

        //Clicar no link com o texto "SIGN IN"
        formSigninBox.findElement(By.linkText("SIGN IN")).click();

        //Validar se o texto do elemento class "me", validar se está "Hi, Julio"
        WebElement me = navegador.findElement(By.className("me"));
        String textonoElementoMe = me.getText();
        //assertEquals("Hi, Julio", textonoElementoMe);

        //Clicar em clique com class "me"
        navegador.findElement(By.className("me")).click();

        //Clicar no link com o texto "MORE DATA ABOUT YOU"
        navegador.findElement(By.linkText("MORE DATA ABOUT YOU")).click();

    }
   // @Test
    public void testAdicionaInformacaoAdicionalUsuario(){



        //Clicar no botão através do xpath //button[@data-target="addmoredata"]
        navegador.findElement(By.xpath("//button[@data-target=\"addmoredata\"]")).click();

        //Identificar a popup onde está o formulario de id addmoredata
        WebElement popUpAddMoreData = navegador.findElement(By.id("addmoredata"));

        //Na combo de name type escolher a opção "Phone"
        WebElement campoType = navegador.findElement(By.name("type"));
        new Select(campoType).selectByVisibleText("Phone");

        //No campo de name Contact digitar "+5511999999999"
        popUpAddMoreData.findElement(By.name("contact")).sendKeys("+5511999999999");

        //Clicar em salvar no link de text "SAVE"
        popUpAddMoreData.findElement(By.linkText("SAVE")).click();

        //Na mensagem de id "toast-container" validar que o texto é "Your contact has been added!"
        WebElement mensagemPop = navegador.findElement(By.id("toast-container"));
        String mensagem = mensagemPop.getText();
        assertEquals("Your contact has been added!",mensagem);

    }

    @Test
    public void RemoverContatoUsuario(){
        //Clicar no elemento pelo seu xpath "//span[text()="numerotelefone"]/following-sibling::a"
        navegador.findElement(By.xpath("//span[text()=\"+5511989893344\"]/following-sibling::a")).click();

        //Confirmar janela javascript
        navegador.switchTo().alert().accept();

        //Validar que a mensagem apresentada foi "Rest in peace, dear phone!"
        WebElement mensagemPop = navegador.findElement(By.id("toast-container"));
        String mensagem = mensagemPop.getText();
        assertEquals("Rest in peace, dear phone!",mensagem);

        //Aguardar até 10 segundos para a janela desaparecer
        WebDriverWait Aguardar = new WebDriverWait(navegador,10);
        Aguardar.until(ExpectedConditions.stalenessOf(mensagemPop));

        //Clicar no link com texto "Logout"
        navegador.findElement(By.linkText("Logout")).click();

    }

    @After
    public void TearDown(){
        //Fechar Navegador
     //   navegador.quit();
    }
}
